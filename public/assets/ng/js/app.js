
var app=angular.module('SimpegApp',['ngRoute','ngMessages','ngTable']);

app.config(function($routeProvider){
    $routeProvider.when('/',{
    templateUrl : 'pages/master_ektp.php',
    controller : 'EktpController'
    })
     .when('/pegawai',{  
        templateUrl : 'pages/master_pegawai.php',
        controller : 'PegawaiController'
            })
                    .otherwise({
                        redirectTo:'/'
                    })      
            
});

app.constant("CSRF_TOKEN", '{!! csrf_token() !!}');
app.factory("FooService", function($http, CSRF_TOKEN) {
    console.log(CSRF_TOKEN);
});

app.service('MasterEktpService',['$http',function($http){
        this.index=function() {
            return $http.get('http://localhost/training-angularjs/bab5/api/services.php?func=ektp');
        }
}]);

app.service('MasterAgamaService',['$http',function($http){
        this.index=function() {
            return $http.get('http://localhost/training-angularjs/bab5/api/services.php?func=agama');
        }
}]);

app.service('MasterUnitKerjaService',['$http',function($http){
        this.index=function() {
            return $http.get('http://localhost/training-angularjs/bab5/api/services.php?func=unitkerja');
        }
}]);

app.service('MasterJabatanService',['$http',function($http){
        this.index=function() {
            return $http.get('http://localhost/training-angularjs/bab5/api/services.php?func=jabatan');
        }
}]);


app.service('LoginService',['$http',function($http){
        this.index=function(send) {
            return $http.get('http://localhost/usermanagement/public/pegawai/login/'+send.nip+'/'+send.password);
        }
}]);


      