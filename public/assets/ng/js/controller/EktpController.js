
app.controller('EktpController',['$scope','$http','MasterEktpService','MasterAgamaService','NgTableParams','LoginService','CSRF_TOKEN',
    function($scope,$http,MasterEktpService,MasterAgamaService,NgTableParams,LoginService,CSRF_TOKEN){
        
    $scope.menu = 'ektp';
    $scope.title = 'Master Ektp';
    $scope.message = 'Pengelolaan Data KTP Pegawai';
    $scope.loader   = true;
    $scope.alert = false;
    $scope.msg = {};
    

      var send = {'nip':2,'password':'admin'};
      
      LoginService.index(send).success(function(data){
            $scope.session_ektp = data;

    });
      
      
//      $scope.data = [{ name: 'christian', age: 21 }, { name: 'anthony', age: 88 }];
//      $scope.tp = new NgTableParams({}, { dataset: $scope.data});
      
     $scope.showdata = function(){
     MasterEktpService.index().success(function(data){
         $scope.ektp = data;
          $scope.loader   = false; 
        
    });
     };
    
    $scope.showdata();
    
    MasterAgamaService.index().success(function(data){
         $scope.agama = data;
        
    });
   
    $scope.dataform = {};
   
    $scope.formAction = function(action,nik,index){
           
            $scope.action = action;
            
            if(action == 'edit'){
                $scope.dataform.nik = $scope.ektp[index]['nik'];
                $scope.dataform.nama    = $scope.ektp[index]['nama'];
                $scope.dataform.jenis_kelamin    = $scope.ektp[index]['jenis_kelamin'];
                $scope.dataform.master_agama_id  = $scope.ektp[index]['master_agama_id'];
                $scope.dataform.alamat  = $scope.ektp[index]['alamat'];
                
            }else if(action == 'add'){
                $scope.kosongkandataform();
            }
        };

        $scope.kosongkandataform = function(){
                $scope.dataform.nik = '';
                $scope.dataform.nama    = '';
                $scope.dataform.jenis_kelamin    = '';
                $scope.dataform.master_agama_id  = '';
                $scope.dataform.alamat  = '';
        }
        
        var urlserver = "http://localhost/training-angularjs/bab5/api/services.php?func=ektp";
        $scope.save = function() {
           
            if($scope.action==null) {
                $scope.action = 'add';
            }
                
            $http.post(urlserver+'&action='+$scope.action,$scope.dataform).success(function(response){
               if(response){
                   $scope.showdata();
                    $scope.kosongkandataform();
                   //alert('Data '+$scope.dataform.nama+' Berhasil Di '+$scope.action)
                   $scope.alert = true;
                   $scope.msg = {'type':'success','info':'Data '+$scope.dataform.nama+' Berhasil Di '+$scope.action}
               } 
               else {
                    $scope.alert = true;
                   $scope.msg = {'type':'error','info':'Data Gagal Dientry'}
               }
               document.getElementById('msg_alert').style.display = 'block';
               $('html, body').animate({scrollTop: 0}, 'slow');
            });
            
        };
        
        $scope.delete = function(nik,nama){
            if(confirm('Apakah Anda yakin akan menghapus data KTP dengan nama '+nama+' ?')){
                $http.get(urlserver +'&action=delete&nik='+nik).success(function(response){
                    if(response){
                        $scope.showdata();
                       // alert('Data dengan nama '+ nama +' berhasil dihapus');
                       $scope.alert = true;
                         $scope.msg = {'type':'success','info':'Data dengan nama '+ nama +' berhasil dihapus'}
                    }
                    else {
                        $scope.alert = true;
                         $scope.msg = {'type':'error','info':'Data Gagal Dihapus'}
                    }
                  document.getElementById('msg_alert').style.display = 'block';
                   $('html, body').animate({scrollTop: 0}, 'slow');
                });
            }
        };
        
        
          $scope.debugData = function($index) {
                   alert('test');
            };
            
     
        
}]);