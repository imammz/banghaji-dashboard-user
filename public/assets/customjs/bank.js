 var save_method; //for save method string
    var table;
    $(document).ready(function() {
      table = $('#table').DataTable({ 
        
        "processing": true, //Feature control the processing indicator.
        "serverSide": false, //Feature control DataTables' server-side processing mode.
        
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "Dt_bank/ajax_list",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
          "targets": [ -1 ], //last column
          "orderable": false, //set not orderable
        },
        ],

      });
    });

    function form_bank(bank_id)
    {
      bank_id = typeof bank_id !== 'undefined' ? bank_id : null;
      save_method = 'add';
      $('#contentPage').empty();
      $('#contentPage').load('Dt_bank/form/'+bank_id+'?_=' + (new Date()).getTime());
    }
	
	function backlistbank()
    {
      $('#contentPage').empty();
      $('#contentPage').load('Dt_bank/'+'?_=' + (new Date()).getTime());
        reload_table();
    }

    function reload_table()
    {
      table.ajax.reload(null,false); //reload datatable ajax 
    }
	
	
    function save()
    {
      var url;
          url = "Dt_bank/ajax_proses";
       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form_bank').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               alert('Proses berhasil !');
			   backlistbank();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Proses gagal dilakukan !');
            }
        });
    }

    function delete_bank(id)
    {
      if(confirm('Are you sure delete this data?'))
      {
        // ajax delete data to database
          $.ajax({
            url : "Dt_bank/ajax_delete/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               //if success reload ajax table
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
         
      }
    }