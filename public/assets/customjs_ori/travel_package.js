 var save_method; //for save method string
    var table;
    $(document).ready(function() {
      table = $('#table').DataTable({ 
        
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "Travel_package/ajax_list",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
          "targets": [ -1 ], //last column
          "orderable": false, //set not orderable
        },
        ],

      });
    });
	
    function add_travel_package()
    {
      save_method = 'add';
      $('#contentPage').empty();
      $('#contentPage').load('Travel_package/form'+'?_=' + (new Date()).getTime());
    }
	
	function edit_travel_package(id)
    {
      save_method = 'edit';
      $('#contentPage').empty();
      $('#contentPage').load('Travel_package/form/'+id);
    }
	
	function backlisttp()
    {
      $('#contentPage').empty();
      $('#contentPage').load('Travel_package/'+'?_=' + (new Date()).getTime());
        reload_table();
    }

    function reload_table()
    {
      table.ajax.reload(null,false); //reload datatable ajax 
    }

    function save()
    {
      var url;
      if(save_method == 'add') 
      {
          url = "<?php echo site_url('Travel_package/ajax_add')?>";
      }
      else
      {
        url = "<?php echo site_url('Travel_package/ajax_update')?>";
      }

       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               $('#modal_form').modal('hide');
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
    }

    function delete_travel_package(id)
    {
      if(confirm('Are you sure delete this data?'))
      {
        // ajax delete data to database
          $.ajax({
            url : "<?php echo site_url('Travel_package/ajax_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               //if success reload ajax table
               $('#modal_form').modal('hide');
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
         
      }
    }
	
	
	// FASILITAS TOUR //
	
	$(document).ready(function() {
      table = $('#table-tour').DataTable({ 
        
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "Travel_package/ajax_list_tour",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
          "targets": [ -1 ], //last column
          "orderable": false, //set not orderable
        },
        ],

      });
    });
	
	function add_fasiltas_tour()
    {
      save_method = 'add';
      $('#pageTour').empty();
      $('#pageTour').load('Travel_package/form_tour'+'?_=' + (new Date()).getTime());
    }
	
	function edit_fasilitas_tour(id)
    {
      save_method = 'edit';
      $('#pageTour').empty();
      $('#pageTour').load('Travel_package/form_tour/'+id);
    }
	
	// FASILITAS HOTEL //
	
	$(document).ready(function() {
      table = $('#table-hotel').DataTable({ 
        
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "Travel_package/ajax_list_hotel",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
          "targets": [ -1 ], //last column
          "orderable": false, //set not orderable
        },
        ],

      });
    });
	
	function add_fasiltas_hotel()
    {
      save_method = 'add';
      $('#pageHotel').empty();
      $('#pageHotel').load('Travel_package/form_hotel'+'?_=' + (new Date()).getTime());
    }
	
	function edit_fasilitas_hotel(id)
    {
      save_method = 'edit';
      $('#pageHotel').empty();
      $('#pageHotel').load('Travel_package/form_hotel/'+id);
    }
	
	// FASILITAS PESAWAT //
	
	$(document).ready(function() {
      table = $('#table-pesawat').DataTable({ 
        
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "Travel_package/ajax_list_pesawat",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
          "targets": [ -1 ], //last column
          "orderable": false, //set not orderable
        },
        ],

      });
    });
	
	function add_fasiltas_pesawat()
    {
      save_method = 'add';
      $('#pagePesawat').empty();
      $('#pagePesawat').load('Travel_package/form_pesawat'+'?_=' + (new Date()).getTime());
    }
	
	function edit_fasilitas_pesawat(id)
    {
      save_method = 'edit';
      $('#pagePesawat').empty();
      $('#pagePesawat').load('Travel_package/form_pesawat/'+id);
    }