    var save_method; //for save method string
    var table;
    $(document).ready(function() {

      table = $('#table').DataTable({ 
      
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "person/ajax_list",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
          "targets": [ -1 ], //last column
          "orderable": true, //set not orderable
        },
        ],

      });


    });

    function add_person()
    {
      save_method = 'add';
      $('#contentPage').empty();
      $('#contentPage').load('person/form'+'?_=' + (new Date()).getTime());
    }

    function edit_person(id)
    {
      save_method = 'edit';
      $('#contentPage').empty();
      $('#contentPage').load('person/form/'+id);
    }

    function backlistperson()
    {
      $('#contentPage').empty();
      $('#contentPage').load('person/'+'?_=' + (new Date()).getTime());
        reload_table();
    }

    function reload_table()
    {
      table.ajax.reload(null,false); //reload datatable ajax 
    }

    function save()
    {
      var url;

      url = 'person/ajax_add';
     
      $.ajax({
            url : url,
            type: "POST",
            data: $('#form_person').serialize(),
            dataType: "JSON",

            success: function(data)
            {
              var msg = data.message;

               //if success close modal and reload ajax table

              //$('#message').html("<div class='alert alert-success'><i class='fa fa-check-circle-o'></i>"+msg+"</div>").delay(3000).fadeOut('slow');
              alert('Proses berhasil !');
              $('#contentPage').empty();
              $('#contentPage').load('person/'+'?_=' + (new Date()).getTime());
              reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                $('#message').html("<div class='alert alert-danger'><i class='fa fa-check-circle-o'></i> Proses gagal </div>").delay(3000).fadeOut('slow');
            }
        });

       // ajax adding data to database
          
    }

    function delete_person(id)
    {
      if(confirm('Apakah anda yakin?'))
      {
        // ajax delete data to database
          $.ajax({
            url : "person/ajax_delete/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               //if success reload ajax table
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
         
      }
    }

    function reset_password(id)
    {
      if(confirm('Apakah anda yakin akan mereset password?'))
      {
        // ajax delete data to database
          $.ajax({
            url : "person/ajax_reset/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               //if success reload ajax table
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
         
     
      }
    }

