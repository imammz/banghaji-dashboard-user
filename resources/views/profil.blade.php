<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left">
        <div class="page-title">
            [ <i class="fa fa-info"></i> ] {{ $section or '' }}
        </div>
    </div>
</div>

<div class="page-content">
    <div class="portlet box portlet-green">
        <div class="portlet-body">
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#akun" aria-controls="home" role="tab" data-toggle="tab">Akun</a></li>
                    <li role="presentation"><a href="#biodata" aria-controls="profile" role="tab" data-toggle="tab">Biodata</a></li>
                    <li role="presentation"><a href="#info_pembayaran" aria-controls="messages" role="tab" data-toggle="tab">Info Pembayaran</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="akun">
                        <div class="col-lg-1">
                            <img class="img-responsive" src="{{ asset('assets/images/gallery/media5.jpg') }}" alt="Raisa" width="150">
                        </div>
                        <div class="col-lg-11">
                            <ul class="list-unstyled" style="font-size: 24px;">
                                <li>
                                    Nama Lengkap :
                                </li>
                                <li>
                                    Email :
                                </li>
                                <li>
                                    Last Login :
                                </li>
                            </ul>
                        </div>

                        <div class="text-center">
                            <h2>
                                Bang Haji Code Aktif :
                            </h2>
                            <h1>
                                B10495A
                            </h1>
                            Perjalanan Umroh tanggal 3/01/16 sd 14/01/16 <br>
                            Bersama Bismillah Travel

                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="biodata">
                            <ul class="list-unstyled" style="font-size: 24px;">
                                <li>
                                    Nama  :
                                </li>
                                <li>
                                    NIK :
                                </li>
                                <li>
                                    Tempat Lahir :
                                </li>
                                <li>
                                    Tanggal Lahir :
                                </li>
                            </ul>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="info_pembayaran">
                        <h2>Bank Transfer</h2>
                        <ul class="list-unstyled" style="font-size: 18px;">
                            <li>
                                Bank  :
                            </li>
                            <li>
                                Kantor Cabang :
                            </li>
                            <li>
                                Atas Nama :
                            </li>
                            <li>
                                Nomor Rekening :
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>