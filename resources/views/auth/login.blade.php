@extends('layouts.login')

@section('login')
    <div class="page-form logs">

        <form action="{{ url('login') }}" class="form" method="post">
            <div class="header-content">
                <h1>Log In</h1>
                <img class="login-logo" src="{{ asset('assets/images/gallery/Logo_BH_1.png') }}">
            </div>

            <div class="body-content">
                <div class="form-group">
                    <div class="input-icon right"><i class="fa fa-user"></i><input type="text" placeholder="Username" name="username" class="form-control"></div>
                </div>
                <div class="form-group">
                    <div class="input-icon right"><i class="fa fa-key"></i><input type="password" placeholder="Password" name="password" class="form-control"></div>
                </div>
                <div class="form-group pull-left">
                    <div class="checkbox-list"><label><input type="checkbox">&nbsp;
                            Keep me signed in</label>
                    </div>
                </div>
                <div class="form-group pull-right"><button type="submit" class="btn btn-success">Log In
                        &nbsp;<i class="fa fa-chevron-circle-right"></i></button>
                </div>
                <div class="clearfix"></div>
                <div class="forget-password">
                    <h4>Forgotten your Password?</h4>
                    <p>no worries, click <a href='#' class='btn-forgot-pwd'>here</a> to reset your password.</p>
                </div>
                <hr>
                <p>Don't have an account? <a id="btn-register" href="extra-signup.html">Register Now</a></p>
            </div>
        </form>
    </div>
@endsection
