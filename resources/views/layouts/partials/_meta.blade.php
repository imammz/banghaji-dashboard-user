<meta charset="UTF-8">
<meta name="viewport"
      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<!--Loading bootstrap css-->
<link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-datepicker/css/datepicker3.css') }}">
<link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,700">
<link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald:400,700,300">
<link type="text/css" rel="stylesheet" href="{{ asset('assets/vendors/jquery-ui-1.10.4.custom/css/ui-lightness/jquery-ui-1.10.4.custom.min.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('assets/vendors/font-awesome/css/font-awesome.min.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('assets/vendors/bootstrap/css/bootstrap.min.css') }}">
<!--LOADING STYLESHEET FOR PAGE-->
<link type="text/css" rel="stylesheet" href="{{ asset('assets/vendors/intro.js/introjs.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('assets/vendors/calendar/zabuto_calendar.min.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('assets/vendors/sco.message/sco.message.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('assets/vendors/intro.js/introjs.css') }}">
<!--Loading style VENDORS-->
<link type="text/css" rel="stylesheet" href="{{ asset('assets/vendors/animate.css/animate.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('assets/vendors/jquery-pace/pace.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('assets/vendors/iCheck/skins/all.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('assets/vendors/jquery-notific8/jquery.notific8.min.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker-bs3.css') }}">
<!--Loading style-->
<link type="text/css" rel="stylesheet" href="{{ asset('assets/css/themes/style1/orange-blue.css') }}" class="default-style">
<link type="text/css" rel="stylesheet" href="{{ asset('assets/css/themes/style1/orange-blue.css') }}" id="theme-change" class="style-change color-change">
<link type="text/css" rel="stylesheet" href="{{ asset('assets/css/style-responsive.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('assets/vendors/DataTables/media/css/jquery.dataTables.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('assets/vendors/DataTables/media/css/dataTables.bootstrap.css') }}">