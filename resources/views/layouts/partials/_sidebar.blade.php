<div class="sidebar-collapse menu-scroll">

    <ul id="side-menu" class="nav">
        <li class="user-panel">
            <div class="thumb">
                <img src="{{ asset('assets/images/gallery/Logo_BH_1.png') }}" alt="" class="img-circle"/>
            </div>
            <div class="info">
                <p id="current_user">{{ isset(Auth::user()->nama_lengkap) ? Auth::user()->nama_lengkap : '-' }}</p>
                <ul class="list-inline list-unstyled">

                </ul>
            </div>
            <div class="clearfix">
            </div>
        </li>
        <li class="">
            <a href="#" onclick="getMenu('{{ url('beranda') }}')">
                <i class="fa fa-home fa-fw">
                    <div class="icon-bg bg-orange">
                    </div>
                </i>
                <span class="menu-title">
                    Home
                </span>
            </a>
        </li>
        <li class="">
            <a href="#" onclick="getMenu('{{ url('profil') }}')">
                <i class="fa fa-user fa-fw">
                    <div class="icon-bg bg-orange">
                    </div>
                </i>
                <span class="menu-title">
                Profil
                </span>
            </a>
        </li>
        <li class="">
            <a href="#" onclick="getMenu('{{ url('pemesanan') }}')">
                <i class="fa fa-list fa-fw">
                    <div class="icon-bg bg-orange">
                    </div>
                </i>
                <span class="menu-title">
                Pemesanan
                </span>
            </a>
        </li>
        <li class="">
            <a href="#" onclick="getMenu('{{ url('perjalanan') }}')">
                <i class="fa fa-tachometer fa-fw">
                    <div class="icon-bg bg-orange">
                    </div>
                </i>
                <span class="menu-title">
                Perjalanan Saya
                </span>
            </a>
        </li>
    </ul>


</div>

<script type="text/javascript" src="{{ asset('assets/customjs/getMenu.js') }}"></script>