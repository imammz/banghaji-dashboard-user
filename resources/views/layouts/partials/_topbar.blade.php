<a id="menu-toggle" href="#" class="hidden-xs">
    <i class="fa fa-bars">
    </i>
</a>
<ul class="nav navbar navbar-top-links navbar-right mbn">
    <li id="topbar-chat" class="hidden-xs">
        <marquee direction="left" width="650px" style="color:white">
            <a href="javascript:void(0)" class="btn-chat marquee-text">
                Bang Haji Dashboard | Cara Kini Ke Tanah Suci
            </a>
        </marquee>
    </li>
    <li class="dropdown topbar-user">
        <a data-hover="dropdown" href="#" class="dropdown-toggle">
        <!--<img src="{{ asset('assets/images/gallery/Logo_BH_1.png') }}" alt="" class="img-responsive img-circle"/>-->
            &nbsp;
            <span class="hidden-xs">
                @if(Auth::guest())
                    {{ 'Raisa' }}
                    @else
                    {{ Auth::user()->nama_lengkap }}
                @endif
            </span>
            &nbsp;
            <span class="caret">
            </span>
        </a>
        <ul class="dropdown-menu dropdown-user pull-right">
            <li>
                <a href="#">
                    <i class="fa fa-user">
                    </i>
                    Profil
                </a>
            </li>
            <li>
                <a href="{{ url('/logout') }}"
                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="fa fa-key"></i>Logout
                </a>

                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
        </ul>
    </li>
</ul>