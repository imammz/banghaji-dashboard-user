<!doctype html>
<html lang="en">
<head>
    <title>{{ config('app.name') }}</title>
    @include('layouts.partials._meta')
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
</head>
<body>

@if(Request::is('login'))
    @yield('login')
@else
    <div>
        <div class="news-ticker bg-red">
            <div class="container">
                <a id="news-ticker-close" href="javascript:;">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>

        <!--BEGIN BACK TO TOP-->
        <a id="totop" href="#">
            <i class="fa fa-angle-up">
            </i>
        </a>
        <!--END BACK TO TOP-->
        <!--BEGIN TOPBAR-->
        <div id="header-topbar-option-demo" class="page-header-topbar">
            <nav id="topbar" role="navigation" style="margin-bottom: 0; z-index: 2;" class="navbar navbar-default navbar-static-top">
                <div class="navbar-header">
                    <button type="button" data-toggle="collapse" data-target=".sidebar-collapse" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a id="logo" href="#" class="navbar-brand">
                        <span class="fa fa-rocket">
                        </span><span class="logo-text">
                            <small>BANG HAJI</small>
                        </span>
                        <span style="display: none" class="logo-text-icon">
                        </span>
                    </a>
                </div>
                <div class="topbar-main">
                    @include('layouts.partials._topbar')
                </div>
            </nav>
        </div>
        <!--END TOPBAR-->

        <div id="wrapper">
            <!--BEGIN SIDEBAR MENU-->
            <nav id="sidebar" role="navigation" class="navbar-default navbar-static-side">
                @include('layouts.partials._sidebar')
            </nav>
            <!--END SIDEBAR MENU-->

            <!--BEGIN PAGE WRAPPER-->
            <div id="page-wrapper">

                <div id="contentPage">

                    @yield('content')
                    <iframe width="100%" style="min-height: 790px"
                            src="">
                    </iframe>
                </div>
                <!--BEGIN TITLE & BREADCRUMB PAGE-->



                <!--END TITLE & BREADCRUMB PAGE-->
                <!--BEGIN CONTENT-->


                <!--END CONTENT-->
            </div>

            <!--BEGIN FOOTER-->
            <div id="footer">
                @include('layouts.partials._footer')
            </div>
            <!--END FOOTER-->

            <!--END PAGE WRAPPER-->
        </div>
    </div>

@endif

@include('layouts.partials._js')
</body>
</html>