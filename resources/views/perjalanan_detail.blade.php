<link type="text/css" rel="stylesheet" href="{{ asset('assets/vendors/lightbox/css/lightbox.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('assets/vendors/fullcalendar/fullcalendar.css') }}">
<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left">
        <div class="page-title">
            [ <i class="fa fa-info"></i> ] {{ $section or '' }}
        </div>
    </div>
</div>
<div class="page-content">
    <div class="portlet box portlet-green">
        <div class="portlet-header">
            <div class="caption">Paket Umroh Ceria</div>
        </div>
        <div class="portlet-body">
            <div>

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#gallery" aria-controls="home" role="tab" data-toggle="tab">Gallery</a></li>
                    <li role="presentation"><a href="#jadwal" aria-controls="jadwal" role="tab" data-toggle="tab">Jadwal Kegiatan</a></li>
                    <li role="presentation"><a href="#perjalanan" aria-controls="perjalanan" role="tab" data-toggle="tab">Info Perjalanan</a></li>
                    <li role="presentation"><a href="#pemesanan" aria-controls="pemesanan" role="tab" data-toggle="tab">Info Pemesanan</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="gallery">
                        <div class="gallery-pages">
                            <ul style="float: left;" class="list-filter list-unstyled">
                                <li data-filter="all" class="filter active">All</li>
                                {{--<li data-filter=".development" class="filter">Development</li>--}}
                            </ul>
                            <div class="clearfix"></div>
                            <div class="row mix-grid">
                                <div class="col-md-3 mix">
                                    <div class="hover-effect">
                                        <div class="img"><img src="http://swlabs.co/madmin/code/images/gallery/1.jpg" alt="" class="img-responsive"/></div>
                                        <div class="info">
                                            <h3>Pellentesque vehicula</h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 mix">
                                    <div class="hover-effect">
                                        <div class="img"><img src="http://swlabs.co/madmin/code/images/gallery/13.jpg" alt="" class="img-responsive"/></div>
                                        <div class="info">
                                            <h3>Pellentesque vehicula</h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 mix">
                                    <div class="hover-effect">
                                        <div class="img"><img src="http://swlabs.co/madmin/code/images/gallery/3.jpg" alt="" class="img-responsive"/></div>
                                        <div class="info">
                                            <h3>Pellentesque vehicula</h3>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 mix">
                                    <div class="hover-effect">
                                        <div class="img"><img src="http://swlabs.co/madmin/code/images/gallery/5.jpg" alt="" class="img-responsive"/></div>
                                        <div class="info">
                                            <h3>Pellentesque vehicula</h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 mix">
                                    <div class="hover-effect">
                                        <div class="img"><img src="http://swlabs.co/madmin/code/images/gallery/16.jpg" alt="" class="img-responsive"/></div>
                                        <div class="info">
                                            <h3>Pellentesque vehicula</h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 mix">
                                    <div class="hover-effect">
                                        <div class="img"><img src="http://swlabs.co/madmin/code/images/gallery/6.jpg" alt="" class="img-responsive"/></div>
                                        <div class="info">
                                            <h3>Pellentesque vehicula</h3>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 mix">
                                    <div class="hover-effect">
                                        <div class="img"><img src="http://swlabs.co/madmin/code/images/gallery/14.jpg" alt="" class="img-responsive"/></div>
                                        <div class="info">
                                            <h3>Pellentesque vehicula</h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 mix">
                                    <div class="hover-effect">
                                        <div class="img"><img src="http://swlabs.co/madmin/code/images/gallery/8.jpg" alt="" class="img-responsive"/></div>
                                        <div class="info">
                                            <h3>Pellentesque vehicula</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="jadwal">
                        <div id="calendar"></div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="perjalanan">
                        <h3><b>Berangkat Dari Jakarta</b></h3>
                        <ul class="list-unstyled" style="font-size: 18px;">
                            <li>Maskapai Berangkat :</li>
                            <li>Hotel di Jeddah :</li>
                            <li>Perjalanan ke Madinah :</li>
                            <li>Hotel di Madinah :</li>
                        </ul>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="pemesanan">
                        <h3><b>Info Pemesanan</b></h3>
                        <ul class="list-unstyled" style="font-size: 18px;">
                            <li>Tanggal Pemesanan :</li>
                            <li>Tanggal Konfirmasi :</li>
                            <li>Total Biaya Paket Umroh :</li>
                            <li>Pembayaran Melalui :</li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


<!--LOADING SCRIPTS FOR PAGE-->
<script src="{{ asset('assets/vendors/mixitup/src/jquery.mixitup.js') }}"></script>
<script src="{{ asset('assets/vendors/lightbox/js/lightbox.min.js') }}"></script>
<script src="{{ asset('assets/js/page-gallery.js') }}"></script>
<script src="{{ asset('assets/vendors/fullcalendar/fullcalendar.min.js') }}"></script>
<script src="{{ asset('assets/js/page-calendar.js') }}"></script>