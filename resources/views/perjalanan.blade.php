<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left">
        <div class="page-title">
            [ <i class="fa fa-info"></i> ] {{ $section or '' }}
        </div>
    </div>
</div>

<div class="page-content">
    <div class="portlet box portlet-green">
        <div class="portlet-body">
            <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-8 blog-articles">
                    <h1>Riwayat Perjalanan Haji/Umroh Anda</h1>
                    <div class="row">
                        <div class="col-md-4 blog-img">
                            <img src="{{ asset('assets/images/gallery/1.jpg') }}" alt="" class="img-responsive"/>
                            <ul class="list-inline blog-date">
                                <li><i class="fa fa-calendar fa-fw"></i><a href="#">3 December 2016</a></li>
                            </ul>
                        </div>
                        <div class="col-md-8 blog-article">
                            <h3><a href="#">Paket Umroh Ceria</a></h3>
                            <p>Paket Umroh Plus Serta disertai dengan wisata rohani ke Mesir</p>
                            <a href="#" onclick="getMenu('{{ url('perjalanan/detail') }}')" class="btn btn-green">Selengkapnya
                                &nbsp;<i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        <div class="col-md-4 blog-img">
                            <img src="{{ asset('assets/images/gallery/2.jpg') }}" alt="" class="img-responsive"/>
                            <ul class="list-inline blog-date">
                                <li><i class="fa fa-calendar fa-fw"></i><a href="#">16 December 2016</a></li>
                            </ul>
                        </div>
                        <div class="col-md-8 blog-article">
                            <h3><a href="#">Paket Umroh Plus</a></h3>
                            <p>Paket Umroh Menjelang 1 Ramadhan</p>
                            <a href="#" onclick="getMenu('{{ url('perjalanan/detail') }}')" class="btn btn-green">Selengkapnya
                                &nbsp;<i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>