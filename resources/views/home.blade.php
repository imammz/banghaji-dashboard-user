@extends('layouts.app')

@section('content')
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">
                [ <i class="fa fa-info"></i> ] {{ $section or '' }}
            </div>
        </div>
    </div>

    <div class="page-content">
        <div class="portlet box portlet-green">
            <div class="portlet-body">
                <div id="sum_box" class="row mbl">
                    <div class="col-sm-6 col-md-3">
                        <div class="panel income db mbm">
                            <div class="panel-body">
                                <p class="icon">
                                    <i class="icon fa fa-share">
                                    </i>
                                </p>
                                <h2 class="description">
                                    Pemesanan Terakhir
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="panel income db mbm">
                            <div class="panel-body">
                                <p class="icon">
                                    <i class="icon fa fa-book">
                                    </i>
                                </p>
                                <h4 class="value">
                            <span>
                                Umroh Tanggal
                            </span>
                                </h4>
                                <p class="description">
                                    20/20/2017
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="panel income db mbm">
                            <div class="panel-body">
                                <p class="icon">
                                    <i class="icon fa fa-upload">
                                    </i>
                                </p>
                                <h3 class="description">
                                    Upload Gallery Perjalanan
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="panel income db mbm bg-green">
                            <div class="panel-body">
                                <h5 class="value">
                                    Bang Haji Code Aktif
                                </h5>
                                <h3 class="description">
                                    4104952
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>


                <h2>Selamat Datang Raisa</h2>
                <p class="">
                    Ini Merupakan Halaman Dashboard Bang Haji. Disini anda bisa melakukan pergantian profil berupa data akun, biodata, ganti password,
                    juga bisa melakukan tindak lanjut dari pemesanan anda yang belum selesai.
                </p>
                <p class="">
                    Dasboard ini juga dilengkapi halaman untuk melihat riwayat perjalanan umroh anda selama menggunakan aplikasi Bang Haji
                </p>
            </div>
        </div>
    </div>
@endsection

