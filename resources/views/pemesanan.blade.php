<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left">
        <div class="page-title">
            [ <i class="fa fa-info"></i> ] {{ $section or '' }}
        </div>
    </div>
</div>

<div class="page-content">
    <div class="portlet box portlet-green">
        <div class="portlet-header">
            <div class="caption">
                Pemesanan
            </div>
        </div>
        <div class="portlet-body">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#pemesanan_terakhir" aria-controls="home" role="tab" data-toggle="tab">Pemesanan Terakhir</a></li>
                <li role="presentation"><a href="#riwayat_pemesanan" aria-controls="profile" role="tab" data-toggle="tab">Riyawat Pemesanan</a></li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="pemesanan_terakhir">
                    <h1>Paket Promo V</h1>
                    <p class="description">Tanggal <b>3/01/2016</b> sd <b>14/01/2016</b> </p>

                    <div class="text-center">
                        <h1 class="text-green">
                            Menunggu Konfirmasi Pembayaran
                        </h1>
                        <p>Lakukan konfirmasi pembayaran Melalui Kartu Kredit atau
                            <br>nomor rekening Bank XXX : ********  atau Bank YYYY : *******
                            <br>
                            Kemudian segera lakukan konfirmasi
                        </p>
                        <a href="" class="btn btn-green btn-sm">Klik Untuk Melakukan Konfirmasi Pembayaran</a>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="riwayat_pemesanan">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>Paket Pemesanan</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>20/10/2016</td>
                            <td>Maktour</td>
                            <td>Aktif</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>20/10/2016</td>
                            <td>Maktour</td>
                            <td>Aktif</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>